import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

//import the "useNavigate" hook from react-router-dom and redirect the user back to the "Courses" page after enrolling to a course.

export default function CourseView(){

	//Consume the "User" context object to be able to obtain the user ID so we can enroll a user
	const { user } = useContext(UserContext);

	// Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
	const navigate = useNavigate();

	//Retrieve the "courseId" via the url using the "useParams" hook from react-router-dom and create a "useEffect" hook to check if the courseId is retrieved properly
	const { courseId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);


	//"enroll" function that will enroll a user to a specific course and bind it to the "Enroll" button
	const enroll = (courseId) =>{

		fetch('http://localhost:4000/users/enroll',{
			method:'POST',
			headers:{
				'Content-Type':'application/json',
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res=>res.json())
		.then(data=>{
			//conditional statement that will alert the user of a successful/failed enrollment
			if(data===true){

				Swal.fire({
				  title: "Enrollment Successful!",
				  icon: "success",
				  text: "Thank you for enrolling!"
				});
				// redirect the user to a different page and is an easier approach
				navigate("/courses")

			}else{

				Swal.fire({
				  title: "Something went wrong!",
				  icon: "error",
				  text: "Check your credentials!"
				});

			}

		})
	}

	useEffect(()=>{
		//fetch request that will retrieve the details of the course from our database to be displayed in the "CourseView" page
		console.log(courseId);
		fetch(`http://localhost:4000/courses/${courseId}`)
		.then(res=>res.json())
		.then(data=>{
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	},[courseId])

	return(
		<Container className="mt-5">
		  <Row>
		     <Col lg={{span:6, offset:3}}>
				<Card>
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>PHP {price}</Card.Text>
						<Card.Text>Class Schedule</Card.Text>
						<Card.Text>8:00AM to 5:00PM</Card.Text>
						{/*conditionally render the enroll button if a user is logged in and a button that will redirect the a user to the "Login" page if they are not logged in*/}
						{
							(user.id!==null)?
							<Button variant="primary" onClick={()=>enroll(courseId)}>Enroll</Button>
							:
							<Link className="btn btn-danger" to="/login">Log in to Enroll</Link>
						}
						
					</Card.Body>
				</Card>
		     </Col>
		  </Row>
		</Container>
	)
}
